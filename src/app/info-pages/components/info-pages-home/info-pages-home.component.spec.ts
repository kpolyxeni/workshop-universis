import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoPagesHomeComponent } from './info-pages-home.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('InfoPagesHomeComponent', () => {
  let component: InfoPagesHomeComponent;
  let fixture: ComponentFixture<InfoPagesHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoPagesHomeComponent ],
      imports: [ RouterTestingModule  ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoPagesHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
