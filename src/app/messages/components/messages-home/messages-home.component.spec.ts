import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesHomeComponent } from './messages-home.component';
import {RouterModule} from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';

describe('MessagesHomeComponent', () => {
  let component: MessagesHomeComponent;
  let fixture: ComponentFixture<MessagesHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterModule.forRoot([])
      ],
      declarations: [ MessagesHomeComponent ],
      providers: [
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        }
      ]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
