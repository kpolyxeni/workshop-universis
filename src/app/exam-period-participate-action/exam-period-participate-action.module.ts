import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExamPeriodParticipateActionRoutingModule } from './exam-period-participate-action-routing.module';
import { ApplyComponent } from './apply/apply.component';
import { ModifyComponent } from './modify/modify.component';
import { PreviewComponent } from './preview/preview.component';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { SharedModule } from '@universis/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { environment } from '../../environments/environment';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MostModule,
    SharedModule,
    TranslateModule,
    ExamPeriodParticipateActionRoutingModule
  ],
  declarations: [ApplyComponent, ModifyComponent, PreviewComponent]
})
export class ExamPeriodParticipateActionModule {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch( err => {
      console.error('An error occurred while loading ExamPeriodParticipateActionModule.');
      console.error(err);
    });
  }

  // tslint:disable-next-line: use-life-cycle-interface
  async ngOnInit() {
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`./i18n/exam-period-participate-action.${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);
  }

 }
