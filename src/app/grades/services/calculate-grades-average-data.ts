/* tslint:disable:quotemark */
export const grades = {
  "value": [
    {
      "id": "788543BA-17EE-4F2A-A061-5B38925FBA00",
      "grade1": 0.2,
      "grade2": null,
      "examGrade": 0.2,
      "percentileRank": null,
      "course": {
        "units": 4,
        "courseStructureType": 1,
        "ects": 5,
      },
      "isPassed": 0
    },
    {
      "id": "C2CE466D-8D22-49F0-8D52-EB53536CADA5",
      "grade1": 0.83,
      "grade2": null,
      "examGrade": 0.83,
      "course": {
        "units": 4,
        "courseStructureType": 1,
        "ects": 5,
      },
      "isPassed": 1
    },
    {
      "id": "80D87D72-FA1E-454F-83CE-EE6DCE6BCA5D",
      "grade1": 0.66,
      "grade2": null,
      "examGrade": 0.66,
      "percentileRank": null,
      "course": {
        "units": 4,
        "courseStructureType": 1,
        "ects": 5,
      },
      "isPassed": 1
    },
    {
      "id": "9016BCAA-47EE-4FD2-92C7-80321D1F316E",
      "grade1": 0,
      "grade2": null,
      "examGrade": 0,
      "course": {
        "units": 4,
        "courseStructureType": 1,
        "ects": 5,
      },
      "isPassed": 0
    },
    {
      "id": "B6A943F9-47B4-463E-8AC9-56F77ADC2483",
      "grade1": 0.3,
      "grade2": null,
      "examGrade": 0.3,
      "course": {
        "units": 4,
        "courseStructureType": 1,
        "ects": 5,
      },
      "isPassed": 0
    }
  ]
};

export const courseGrades = {
  "value": [
    {
      "id": "353C0F32-1B9D-42A5-A32D-7B961068D336",
      "courseTitle": "ΜΑΚΡΟΟΙΚΟΝΟΜΙΚΗ ΘΕΩΡΙΑ Ι",
      "units": 0,
      "coefficient": 1,
      "grade": null,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "parentCourse": null,
      "ects": 6,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 0
    },
    {
      "id": "3C643671-CC11-47CA-B9E3-A936AB542195",
      "units": 0,
      "coefficient": 1,
      "grade": null,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "parentCourse": null,
      "ects": 6,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 0
    },
    {
      "id": "B2E557B7-321E-414C-890A-97F42F361F4B",
      "units": 0,
      "coefficient": 1,
      "grade": null,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "parentCourse": null,
      "ects": 6,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 0
    },
    {
      "id": "C8B4D91F-3F79-43DB-B47D-8CCA26909F79",
      "units": 0,
      "coefficient": 1,
      "grade": null,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "parentCourse": null,
      "ects": 6,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 0
    },
    {
      "id": "9989C363-E3B3-4FDD-AE73-A91E8F709AA4",
      "units": 0,
      "coefficient": 1,
      "grade": null,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "parentCourse": null,
      "ects": 6,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 0,
    },
    {
      "id": "089E3842-5757-497A-9B70-528A1DDCCC5E",
      "units": 0,
      "coefficient": 1,
      "grade": 0.61,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "parentCourse": null,
      "ects": 6,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 1,
    },
    {
      "id": "7681A619-26CE-4979-8EFC-0AA09CD61801",
      "units": 0,
      "coefficient": 1,
      "grade": 0.2,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "parentCourse": null,
      "ects": 6,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 0
    },
    {
      "id": "04EF1B38-984B-4596-970B-4445030C82B9",
      "units": 0,
      "coefficient": 1,
      "grade": 0,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "parentCourse": null,
      "ects": 6,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 0
    },
    {
      "id": "9FE5C060-B4DC-4D2E-8322-F7A553DB8E61",
      "units": 0,
      "coefficient": 1,
      "grade": 0.3,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "parentCourse": null,
      "ects": 6,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 0
    },
    {
      "id": "445A01BC-E92E-4395-878E-2EC1046DAF3F",
      "units": 0,
      "coefficient": 1,
      "grade": 0.3,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "parentCourse": null,
      "ects": 6,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 0
    }
  ]
};

export const courseGrades1 = {
  "value": [
    {
      "id": "7036B63B-752A-4B57-B9A5-EBD381502122",
      "coefficient": 1.5,
      "grade": 0.72,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "units": 4,
      "ects": 6,
      "parentCourse": null,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 1
    },
    {
      "id": "E4945324-58CE-4E68-B28B-804C46EFE8BE",
      "coefficient": 1.5,
      "grade": 0.55,
      "calculateUnits": 0,
      "calculateGrade": 1,
      "units": 4,
      "ects": 6,
      "parentCourse": null,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 1
    },
    {
      "id": "A1FA90AB-BAC7-4048-A1FD-BA3460317D0F",
      "coefficient": 2,
      "grade": 0.94,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "units": 15,
      "ects": 10,
      "parentCourse": null,
      "courseStructureType": {
        "id": 4,
        "name": "Complex",
        "isComplete": 1
      },
      "isPassed": 1
    },
    {
      "id": "D87E7D71-9978-4460-BFF1-2B11BAA3BE49",
      "coefficient": 2,
      "grade": 0.94,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "units": 5,
      "ects": 0,
      "parentCourse": 20002601,
      "courseStructureType": {
        "id": 8,
        "name": "Course part",
        "isComplete": 0
      },
      "isPassed": 1
    },
    {
      "id": "D942F5A7-5A5B-4F68-B1CD-6914AB6DA445",
      "coefficient": 2,
      "grade": 0.88,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "units": 5,
      "ects": 0,
      "parentCourse": 20002601,
      "courseStructureType": {
        "id": 8,
        "name": "Course part",
        "isComplete": 0
      },
      "isPassed": 0
    },
    {
      "id": "14B8EBFD-82F2-419B-87C2-EFEECDC566F1",
      "coefficient": 1.5,
      "grade": null,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "units": 3,
      "ects": 2.5,
      "parentCourse": null,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 0
    },
    {
      "id": "B0D102E1-B2B2-4382-B3A5-895556DD64A1",
      "coefficient": 1.5,
      "grade": 0.99,
      "calculateUnits": 0,
      "calculateGrade": 0,
      "units": 4,
      "ects": 5,
      "parentCourse": null,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 1
    },
    {
      "id": "2C646080-7E07-4AFE-B9F8-873AAA2B67AD",
      "coefficient": 1.5,
      "grade": 0.61,
      "calculateUnits": 0,
      "calculateGrade": 1,
      "units": 4,
      "ects": 5,
      "parentCourse": null,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 1
    },
    {
      "id": "12A7E00A-ACCC-4826-AC17-EE478D24A16B",
      "coefficient": 1.5,
      "grade": 0.94,
      "calculateUnits": 1,
      "calculateGrade": 0,
      "units": 4,
      "ects": 5,
      "parentCourse": null,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 1
    },
    {
      "id": "69FB1E9F-3B7D-4791-AD37-9BD67D817008",
      "coefficient": 1.5,
      "grade": null,
      "calculateUnits": 1,
      "calculateGrade": 1,
      "units": 3,
      "ects": 2.5,
      "parentCourse": null,
      "courseStructureType": {
        "id": 1,
        "name": "Simple",
        "isComplete": 1
      },
      "isPassed": 0
    }
  ]
};
